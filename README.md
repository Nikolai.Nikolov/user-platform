## Users


#### Overview
1. We can create users (register), we can edit profile

#### Endpoints

- **GET /users** - get all users
- **POST /users** - {username, about, password} - creates with those required properties when register user. 
- **PUT /users** - {about} - can update the about info.
- **DELETE /users** - we can delete users when logged in.


### 1. Project Requirements

- install dependencies with `npm install`
- setup db configuration:

```js
{
    type: 'mariadb',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: 'parola123',
    database: 'users',
    entities: ["dist/**/*.entity{.ts,.js}"],
    synchronize: true
}
```

- setup `ormconfig.json`

```json
{
  "type": "mysql",
  "host": "localhost",
  "port": 3306,
  "username": "root",
  "password": "parola123",
  "database": "users",
  "synchronize": true,
  "logging": false,
  "entities": [
    "src/models/**/*.entity.ts"
  ],

  "cli": {
    "entitiesDir": "src/models",
  }
}

```

- generate seed data inside nestjs-app: `npm run seed`