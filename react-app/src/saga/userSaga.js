import { call, fork, put, takeLeading } from "redux-saga/effects"
import UsersService from '../services/users-service'
import { updateUsers } from '../actions/users'
import { LOGIN_USER, REGISTER_USER, DELETE_USER, UPDATE_ABOUT_USER } from "../constants/types";

function* onLogInUser({ payload }) {
    const { user, setUser } = payload;
    try {
        yield call(UsersService.login, { user, setUser})
    } catch (e) {
       alert('something went wrong')
    }
}

function* onRegisterUser({ payload }) {
    const { user } = payload;
    try {
        yield call(UsersService.register, { user})
    } catch (e) {
        alert('something went wrong')
    }
}

function* onDeleteUser({ payload }) {
    const { user } = payload;
    try {
        const users = yield call(UsersService.deleteUser,  user.id)
        yield put(updateUsers(users))
    } catch (e) {
        alert('something went wrong')
    }
}

function* onUpdateUser({ payload }) {
    const { id, about } = payload;
    try {
        const users = yield call(UsersService.updateUser, {id, about})
        yield put(updateUsers(users))
    } catch (e) {
        alert('something went wrong')
    }
}

function* onFetchUsers() {
    try {
        const users = yield call(UsersService.getUsers)
        yield put(updateUsers(users))

    } catch (e) {
        alert('something went wrong')
    }
}

function* usersSaga() {
    yield fork(onFetchUsers)
    yield takeLeading(LOGIN_USER, onLogInUser)
    yield takeLeading(REGISTER_USER, onRegisterUser)
    yield takeLeading(DELETE_USER, onDeleteUser)
    yield takeLeading(UPDATE_ABOUT_USER, onUpdateUser)
}

export default usersSaga;