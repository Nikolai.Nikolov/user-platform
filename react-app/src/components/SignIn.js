import React, { useState, useContext } from 'react';
import { Button } from 'react-bootstrap';
import UserContext from '../providers/UserContext';
import { logIn, registerUser } from '../actions/users';
import { connect } from 'react-redux';
import { Nav } from 'react-bootstrap';

const connector = connect((state) => ({
  users: state.users.users
}), {
  logIn,
  registerUser
})

const SignIn = (props) => {
  const location = props.location;
  const { logIn, registerUser } = props;

  const { setUser } = useContext(UserContext);

  const [user, setUserObject] = useState({
    username: '',
    password: '',
    about: ''
  });

  const updateUser = (prop, value) => setUserObject({ ...user, [prop]: value });

  const isLogin = location.pathname.includes('login');

  const loginHandler = () => {
    logIn(user, setUser)
  };

  const registerHandler = () => {
    registerUser(user)
  };

  return (
    <>
      <h1>{isLogin ? 'Login' : 'Register'}</h1>
      <div className='input-login'>
        <label htmlFor="input-username">Username:</label>
        <input type="text" className='username-label' value={user.username} onChange={(e) => updateUser('username', e.target.value)} /><br />
        <label htmlFor="input-password">Password:</label>
        <input type="password" className='password-label' value={user.password} onChange={(e) => updateUser('password', e.target.value)} /><br /><br />
      {isLogin ? null :
        <>
          <label htmlFor="input-about">About:</label>
          <input type="about" className="input-about" value={user.about} onChange={(e) => updateUser('about', e.target.value)} /><br /><br />
        </>
      }
      {isLogin
        ? <Button onClick={loginHandler}><Nav.Link href={`/users`}>Login</Nav.Link></Button>
        : <Button onClick={registerHandler}><Nav.Link href={`/login`}>Create</Nav.Link></Button>
      }
        </div>
    </>
  );
};

export default connector(SignIn);
