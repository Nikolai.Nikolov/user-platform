import React, { useContext } from 'react';
import { useHistory, withRouter } from 'react-router-dom';
import './Navigation.css';
import { Row, Navbar, Nav, Button } from 'react-bootstrap';
import UserContext from '../../providers/UserContext';

const Navigation = () => {
  const { user, setUser } = useContext(UserContext);

  const history = useHistory();


  const logout = () => {
    setUser(null);
    localStorage.removeItem('token');

    history.push('/home');
  }
  return (
    <Row className="Header">
      <Navbar className="full-width">
        <Navbar.Brand>Users App</Navbar.Brand>
        <Nav.Link href="/home">Home</Nav.Link>
        <Nav.Link href="/users">All users</Nav.Link>
        {user
          ? (
            <>
              <Button onClick={logout}>Logout</Button>
            </>
          )
          : <>
            <Nav.Link href="/login">Login</Nav.Link>
            <Nav.Link href="/register">Register</Nav.Link>
          </>
        }
      </Navbar>
    </Row>
  )
};

export default withRouter(Navigation);
