import React from 'react';

const Home = () => {
  return (
    <div>
      <h1>User app</h1>
      <p>
        The users managment app.
      </p>
    </div>
  );
}

export default Home;
