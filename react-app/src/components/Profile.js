import React, { useCallback, useState } from 'react';
import { connect } from 'react-redux';
import { updateUser } from '../actions/users'
import { Button } from 'react-bootstrap';

const connector = connect((state) => ({
  users: state.users.users
}), {
  updateUser
})

const Profile = (props) => {
  const { users, updateUser } = props;
  const match = props.match;

  const [userObj, setUserObject] = useState({
    about: ''
  });

  let user;
  if (match.params.id) {
    user = users.find(u => u.id === +match.params.id);
  }

  const updateUserHandler = useCallback(() => {
    updateUser(user.id, userObj)
  }, [userObj, updateUser, user?.id])

  return (
    <div>
      <h1>Username: {user?.username}</h1>
      <h1>About: {user?.about}</h1>
      <div className='input'>
        <label htmlFor="input-about" className='about-label'>About:</label>
        <input type="text" placeholder="Tell us something about yourself" className='input-text' onChange={(e) => setUserObject(e.target.value)} /><br />
      </div>
      <Button onClick={updateUserHandler}>Update about information </Button>
    </div>
  );
}

export default connector(Profile);
