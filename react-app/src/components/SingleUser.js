import React, { useContext } from 'react';
import UserContext from '../providers/UserContext';
import { withRouter } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { Nav } from 'react-bootstrap';

const SingleUser = (props) => {
    const { user, deleteUserById } = props;

    const userContext = useContext(UserContext);

    const loggedUser = userContext.user;

    const showDeleteBtn = () => {
        if (loggedUser) {
            return <Button onClick={() => deleteUserById(user)}>Delete</Button>;
        }
        return null;
    }

    return (
        user.isDeleted ? null :
            <div className="user">
                <h2>{user.username}</h2>
            <Button> <Nav.Link href={`/profile/${user.id}`}>Show user profile</Nav.Link></Button>
                {showDeleteBtn()}
            </div>
    )
};

SingleUser.defaultProps = {
    user: {},
    deleteUserById: () => { }
}
export default withRouter(SingleUser);
