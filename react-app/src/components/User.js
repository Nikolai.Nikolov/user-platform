import React, { useCallback } from 'react'
import { connect } from 'react-redux';
import SingleUser from './SingleUser';
import { deleteUser } from '../actions/users'


const connector = connect((state) => ({
    users: state.users.users
}), {
    deleteUser
})

const User = (props) => {
    const { users, deleteUser } = props;

    const deleteUserHandler = useCallback((user) => {
        deleteUser(user)
    }, [deleteUser])

    return (
        <>
        {users.length > 0 && users.map((user) => (
                <SingleUser key={user.id} user={user} deleteUserById={deleteUserHandler}/>
            ))}
        </>
    )
}

User.defaultProps = {
    users: []
}

export default connector(User);