import * as type from '../constants/types'

const initialState = {
    users: [],
    loading: false,
    error: null
}

export default function users(state=initialState, actions) {
    switch (actions.type) {
        case type.GET_USERS_REQUESTED:
            return {
                ...state,
                loading: true,
            }
        case type.UPDATE_USERS:
            return {
                ...state,
                loading: false,
                users: actions.payload.users
            }
            default: 
            return state;
    }
}