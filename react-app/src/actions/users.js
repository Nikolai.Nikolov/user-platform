import * as type from '../constants/types'

export const updateUsers = (users) => ({
    type: type.UPDATE_USERS,
    payload: { users }
});

export const logIn = (user, setUser) => ({
    type: type.LOGIN_USER,
    payload: { user, setUser }
})

export const registerUser = (user) => ({
    type: type.REGISTER_USER,
    payload: { user }
})

export const deleteUser = (user) => ({
    type: type.DELETE_USER,
    payload: { user }
})

export const updateUser = (id, about) => ({
    type: type.UPDATE_ABOUT_USER,
    payload: { id, about }
})