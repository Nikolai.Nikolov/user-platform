import React, { useState } from 'react';
import './App.css';
import Profile from './components/Profile';
import Home from './components/Home';
import Navigation from './components/Navigation/Navigation';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import UserContext, { getLoggedUser } from './providers/UserContext';
import SignIn from './components/SignIn';
import User from './components/User'

const App = () => {
  const [user, setUser] = useState(getLoggedUser());

  return (
    <BrowserRouter>
      <UserContext.Provider value={{ user, setUser }}>
        <div className="App">
          <Navigation />
          <Switch>
            <Redirect path="/" exact to="/home" />
            <Route path="/home" component={Home} />
            <Route path="/login" component={SignIn} />
            <Route path="/register" component={SignIn} />
            <Route path="/users" component={User} />
            <Route path="/profile/:id" component={Profile} />
          </Switch>
        </div>
      </UserContext.Provider>
    </BrowserRouter>
  );
}

export default App;
