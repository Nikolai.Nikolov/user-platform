import jwtDecode from "jwt-decode";
import { BASE_URL } from "../constants"
import createRequest, { DELETE, GET, PUT } from "./helper"


const getUsers = () => {
    return createRequest(`${BASE_URL}/users`, GET)
}

const deleteUser = (id) => {
    return createRequest(`${BASE_URL}/users/${id}`, DELETE)
}

const updateUser = (data) => {
    return createRequest(`${BASE_URL}/users/${data.id}`, PUT, data)
}

const login = (data) => {
  if (!data.user.username) {
    return alert('Invalid username!');
  }
  if (!data.user.password) {
    return alert('Invalid password!');
  }
    fetch(`${BASE_URL}/session`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data.user),
      })
        .then(r => r.json())
        .then(result => {
          if (result.error) {
            return alert('try again');
          }
          try {
            const payload = jwtDecode(result.token);
            data.setUser(payload);
            localStorage.setItem('token', result.token);
          } catch (e) {
            return alert('try again');
          }
        })
        .catch(alert('welcome'));
};

const register = (data) => {
  if (!data.user.username) {
    return alert('Invalid username!');
  }
  if (!data.user.password) {
    return alert('Invalid password!');
  }
  if (!data.user.about) {
    return alert('About is empty!');
  }
  fetch(`${BASE_URL}/users`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      ...data.user,
      about: data.user.about,
    }),
  })
    .then(r => r.json())
    .then(result => {
      if (result.error) {
        return alert('try again');
      }

    })
}
const UsersService = {
    getUsers,
    login,
    register,
    deleteUser,
    updateUser
}

export default UsersService;