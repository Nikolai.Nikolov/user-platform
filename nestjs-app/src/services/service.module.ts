import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "src/models/user.entity";
import { TransformService } from "./transform.service";
import { UsersService } from "./users.service";
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from "src/constants/secret";
import { AuthService } from './auth.service';
import { JwtStrategy } from "./strategy/jwt-strategy";
import { Token } from "src/models/token.entity";

@Module({
    imports: [
    TypeOrmModule.forFeature([User, Token]),
    PassportModule,
    JwtModule.register({
        secret: jwtConstants.secret,
        signOptions: {
            expiresIn: '7d',
        },
    })],
    
    providers: [UsersService, TransformService, AuthService, JwtStrategy],
    exports: [UsersService, AuthService]
})
export class ServicesModule {}
