import { Injectable } from '@nestjs/common';
import { UserDTO } from 'src/dtos/user.dto';
import { User } from 'src/models/user.entity';

@Injectable()
export class TransformService {
  public toUserDTO(user: User): UserDTO {
    return {
      id: user.id,
      username: user.username,
      about: user.about,
      isDeleted: user.isDeleted,
    };
  }
}
