import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserDTO } from 'src/dtos/create-user.dto';
import { UpdateUserDTO } from 'src/dtos/update-user.dto';
import { UserDTO } from 'src/dtos/user.dto';
import { User } from 'src/models/user.entity';
import { Repository } from 'typeorm';
import { TransformService } from './transform.service';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    private readonly transformer: TransformService
  ) { }

  async all() {
    
    const users = await this.usersRepository.find({
      where: {
        isDeleted: false,
      },
    });

    return users.map(user => this.transformer.toUserDTO(user));
  }

  async createUser(userDTO: CreateUserDTO): Promise<UserDTO> {
    const user = this.usersRepository.create(userDTO);
    user.password = await bcrypt.hash(user.password, 10);

    const created = await this.usersRepository.save(user);

    return this.transformer.toUserDTO(created);
  }

  async update(userDto: UpdateUserDTO, userId: number): Promise<UserDTO[]> {
    const user = await this.usersRepository.findOne(userId);

    if (!user) {
      throw new Error('No user!')
    }

    user.about = userDto.about;

    await this.usersRepository.update(userId, user);

    return  this.all();
  }

  async deleteUser(userId: number): Promise<UserDTO[]> {
    const user = await this.usersRepository.findOne(userId);
        if (!user) {
            throw new Error('No user!')
        }

    user.isDeleted = true;
    await this.usersRepository.save(user);

    return this.all();
}

}
