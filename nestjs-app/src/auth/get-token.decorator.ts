import { createParamDecorator } from '@nestjs/common';
import { Request } from 'express';

export const GetToken = createParamDecorator((data: any, req: Request) => {

  return (req as any)?.headers?.authorization || (req as any)?.switchToHttp()?.getRequest()?.headers?.authorization;

});
