import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('users')
export class User {
    @PrimaryGeneratedColumn()
    id: number;
    @Column({  type: 'varchar', length: 30 })
    username: string;
    @Column()
    password: string;
    @Column()
    about: string;
    @Column({ default: false })
    isDeleted: boolean;
}