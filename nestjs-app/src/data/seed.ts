import * as bcrypt from 'bcrypt';
import { User } from '../models/user.entity';
import { createConnection, Repository, Connection } from 'typeorm';
import { DbSetup } from '../models/dbsetup.entity';

// Run: `npm run seed` to seed the database

const seedUsers = async (connection: any) => {
  const usersRepo: Repository<User> = connection.manager.getRepository(User);
  
  const users = [{
      username: 'Niki',
      password: await bcrypt.hash('123456', 10),
      about: 'from Plovdiv',
    },
    {
      username: 'basic',
      password: await bcrypt.hash('1234', 10),
      about: 'basic',
    },
    {
      username: 'Pesho',
      password: await bcrypt.hash('1234', 10),
      about: 'from Burgas',
    },
    {
      username: 'Gosho',
      password: await bcrypt.hash('1234', 10),
      about: 'Gosho is 24 years old',
    },
    {
      username: 'Penka',
      password: await bcrypt.hash('1234', 10),
      about: 'Penka loves nestjs',
    },
  ];

  for (const user of users) {
    const foundUser = await usersRepo.findOne({
      username: user.username,
    });

    if (!foundUser) {
      const userToCreate = usersRepo.create(user);
      console.log(await usersRepo.save(userToCreate));
    }
  }

};


const beginSetup = async (connection: Connection) => {
  const dbsetupRepo = connection.manager.getRepository(DbSetup);
  const setupData = await dbsetupRepo.find({});

  if (setupData.length) {
    throw new Error(`Data has been already set up`);
  }
};

const completeSetup = async (connection: Connection) => {
  const dbsetupRepo = connection.manager.getRepository(DbSetup);

  await dbsetupRepo.save({ message: 'Setup has been completed!'});
};

const seed = async () => {
  console.log('Seed started!');
  const connection = await createConnection();

 try {
  await beginSetup(connection);

  await seedUsers(connection);

  await completeSetup(connection);
 } catch (e) {
   console.log(e.message)
 }

  console.log('Seed completed!');
  connection.close();
};

seed().catch(console.error);
