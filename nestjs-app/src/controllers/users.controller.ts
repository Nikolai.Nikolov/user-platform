import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards, ValidationPipe } from '@nestjs/common';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { CreateUserDTO } from 'src/dtos/create-user.dto';
import { UpdateUserDTO } from 'src/dtos/update-user.dto';
import { UserDTO } from 'src/dtos/user.dto';
import { UsersService } from 'src/services/users.service';


@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  // @UseGuards(BlacklistGuard)
  async all() {
    
    return await this.usersService.all();
  }

  @Post()
  async createUser(@Body(new ValidationPipe({ whitelist: true }))  userDTO: CreateUserDTO): Promise<UserDTO> {
    
    return await this.usersService.createUser(userDTO);
  }

  @Put(':id')
  async updateUser(
    @Param('id') userId: string,
    @Body(new ValidationPipe({ whitelist: true })) userDto: UpdateUserDTO,
  ): Promise<UserDTO[]> {

    return await this.usersService.update(userDto, +userId);
  }
  
  @Delete(':id')
  @UseGuards(BlacklistGuard)
  async deleteUser(@Param('id') userId: string) {
    return await this.usersService.deleteUser(+userId);
  }
}
