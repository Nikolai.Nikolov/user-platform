export class JwtPayload {
    id: number;
    about: string;
    username: string;
  }