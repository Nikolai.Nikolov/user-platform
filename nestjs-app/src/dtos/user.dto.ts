export class UserDTO {
  id: number;
  username: string;
  about: string;
  isDeleted: boolean
}
