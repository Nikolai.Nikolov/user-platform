import { IsNotEmpty, IsString } from "class-validator";

export class UpdateUserDTO {
  @IsString()
  @IsNotEmpty()
  about: string;
  }
  